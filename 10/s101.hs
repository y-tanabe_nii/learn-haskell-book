
solveRPN :: String -> Double
solveRPN = head . foldl f [] . words
    where f (x:y:ys) "*" = (y * x):ys
          f (x:y:ys) "+" = (y + x):ys
          f (x:y:ys) "-" = (y - x):ys
          f (x:y:ys) "/" = (y / x):ys
          f (x:y:ys) "^" = (y ** x):ys
          f (x:xs) "ln" = log x:xs
          f xs "sum" = [sum xs]
          f xs numberString = read numberString:xs
