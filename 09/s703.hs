{- list version -}

import System.Environment
import System.Directory
import System.IO
import Control.Exception

main = do
  (fileName1:fileName2:_) <- getArgs
  copy fileName1 fileName2

copy source dest = do
  contents <- readFile source
  bracketOnError
    (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
       hClose tempHandle
       removeFile tempName)
    (\(tempName, tempHandle) -> do
       hPutStr tempHandle contents
       hClose tempHandle
       renameFile tempName dest)

{-

This version can work only for ASCII files.
For example, the following performs base-64 MIME encoding:

	perl -MMIME::Base64 -0777 -ne 'print encode_base64($_)' srcFile > destFile

-}
