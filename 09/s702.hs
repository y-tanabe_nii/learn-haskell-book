import System.Environment
import System.Directory
import System.IO
import Control.Exception
import qualified Data.ByteString.Lazy as B
-- import qualified Data.ByteString as B

main = do
  (fileName1:fileName2:_) <- getArgs
  copy fileName1 fileName2

copy source dest = do
  contents <- B.readFile source
  bracketOnError
    (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
       hClose tempHandle
       removeFile tempName)
    (\(tempName, tempHandle) -> do
       B.hPutStr tempHandle contents
       hClose tempHandle
       renameFile tempName dest)

{--

check:
$ ulimit -v 128000
$ ./s702 /tmp/s1.file /tmp/d1.file 
with /tmp/s1.file around 130MB; for strict and lazy byte strings.

--}
