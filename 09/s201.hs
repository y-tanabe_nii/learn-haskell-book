import System.IO

main = do
  handle <- openFile "201-in.txt" ReadMode
  contents <- hGetContents handle
  putStr contents
  hClose handle
