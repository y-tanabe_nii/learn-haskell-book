import System.Random
import Control.Monad(when)

nmax = 3

main = do
  gen <- getStdGen
  askForNumber gen

askForNumber :: StdGen -> IO ()
askForNumber gen = do
  let (randNumber, newGen) = randomR (1,nmax) gen :: (Int, StdGen)
  putStrLn $ "Which number in the range from 1 to " 
             ++ show nmax ++ " am I thinking of? "
  numberString <- getLine
  when (not $ null numberString) $ do
    let number = read numberString
    if randNumber == number
      then putStrLn "You are correct!"
      else putStrLn $ "Sorry, it was " ++ show randNumber
    askForNumber newGen
