import Data.Char

isPal xs      = xs == reverse xs

isPal'        = isPal . map toLower . filter isAlpha

respondPal xs = if isPal' xs then "yes\n" else "no\n"

main          = interact respondPal
