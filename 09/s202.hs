import System.IO

main = do
  withFile "s20-in.txt" ReadMode $ \handle -> do
         contents <- hGetContents handle
         putStr contents

