import System.Environment
import System.Directory
import System.IO
import Data.List
import Control.Exception  -- This line is necessary for bracketOnError

dispatch          :: String -> [String] -> IO ()
dispatch "add"    = add
dispatch "view"   = view
dispatch "remove" = remove
dispatch "bump"   = bump

main = do
  (command:argList) <- getArgs
  dispatch command argList

add                      :: [String] -> IO ()
add [fileName, todoItem] = modifyTodo fileName f where
  f ss = ss ++ [todoItem]

view            :: [String] -> IO ()
view [fileName] = modifyTodo fileName id

remove                          :: [String] -> IO ()
remove [fileName, numberString] = modifyTodo fileName f where
    f ss = delete (ss !! (read numberString)) ss

bump                          :: [String] -> IO ()
bump [fileName, numberString] = modifyTodo fileName f where
    f ss = item : delete item ss where
        item = ss !! (read numberString)

modifyTodo :: FilePath -> ([String] -> [String]) -> IO ()
modifyTodo fname f = do
  todoTasks <- getToDoTasks fname
  updateFile fname $ unlines $ f todoTasks
  showNumberedTasks fname

getToDoTasks :: FilePath -> IO [String]
getToDoTasks fname = do
  contents <- readFile fname
  return (lines contents)

showNumberedTasks :: FilePath -> IO ()
showNumberedTasks fname = do
  todoTasks <- getToDoTasks fname
  let numberedTasks = zipWith (\n line -> show n ++ " - " ++ line)
                      [0..] todoTasks
  mapM_ putStrLn numberedTasks

updateFile                :: FilePath -> String -> IO ()
updateFile fname newItems = 
  bracketOnError (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
        hClose tempHandle
        removeFile tempName)
    (\(tempName, tempHandle) -> do
        hPutStr tempHandle newItems
        hClose tempHandle
        removeFile fname
        renameFile tempName fname)

