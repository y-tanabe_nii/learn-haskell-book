import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy.Internal as BI

foo = BI.defaultChunkSize

{-

BI.defaultChunkSize

B.pack [99,97,110]
B.pack [98..120]

let by = B.pack [98,111,114,116]
by
B.unpack by

B.fromChunks [S.pack [70,71,72], S.pack [73,74,75], S.pack [76,77,78]]

B.cons 85 $ B.pack [80,81,82,84]

S.cons 85 $ S.pack [80,81,82,84]

B.readFile "todo.txt"
S.readFile "todo.txt"
B.readFile "file1.txt"
S.readFile "file1.txt"

-}
