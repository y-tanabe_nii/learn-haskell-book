import Control.Monad
import Data.Char
import qualified Data.ByteString.Lazy as B

main = do
  str <- B.readFile "file2.txt"
  putStr   "Contents is "
  B.putStr str
  putStrLn $ "Length is " ++ show (B.length str)
