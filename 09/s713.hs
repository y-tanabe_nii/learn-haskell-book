{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Data.Char
import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T

main = do
  str <- T.readFile "file2.txt"
  T.putStr $ append "Contents is " str
  -- "Contents is " is treated as Text, not String
  Prelude.putStrLn $ "Length is " ++ show (T.length str)

