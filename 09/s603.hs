import System.Random

main = do
  showGet
  showGet
  showNew
  showGet
  showGet
  showNew

showGet = showRandom "getStdGen" getStdGen

showNew = showRandom "newStdGen" newStdGen

showRandom name func = do
  putStr name
  putStr "  "
  gen <- func
  putStrLn $ take 20 (randomRs ('a', 'z') gen)

{-
ghc --make s603
./s603
-}
