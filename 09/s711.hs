import Control.Monad
import Data.Char

main = do
  str <- readFile "file2.txt"
  putStr   $ "Contents is " ++ str
  putStrLn $ "Length is " ++ show (length str)
