import System.Random

main = do
  gen <- getStdGen
  putStrLn $ take 20 (randomRs ('a','z') gen)

{-
ghc --make s602
./s602
./s602
-}